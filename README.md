# Ansible Jenkins

A quick project to set up a few Jenkins slaves to for JNLP agent connections.
Primarily used to build maven projects.

## Requirements

* python3
* Python's requests library (pip install requests)
* ansible