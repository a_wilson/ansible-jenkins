#!/usr/bin/env python3

# Versions

NE = "0.18.1" # NE = Node Exporter
MAVEN = "3.6.1"

import requests
from pathlib import Path

def download(url, role, version):
    
    target = f"./roles/{role}/files/{role}-{version}.tar.gz"
    if Path(target).exists():
        print("No download needed.")
        return
    else:
        print(f"Beginning {role} download")

        # TODO: Maybe add try, except here?
        
        r = requests.get(url)
        with open(target, 'wb') as f:
            f.write(r.content)
            print(f"Download success.")

# Download node_exporter binary
ne_url = f"https://github.com/prometheus/node_exporter/releases/download/v{NE}/node_exporter-{NE}.linux-amd64.tar.gz"
download(ne_url, 'node_exporter', NE)

# Download maven binary
m_url = f"http://www.mirrorservice.org/sites/ftp.apache.org/maven/maven-3/{MAVEN}/binaries/apache-maven-{MAVEN}-bin.tar.gz"
download(m_url, 'maven', MAVEN)